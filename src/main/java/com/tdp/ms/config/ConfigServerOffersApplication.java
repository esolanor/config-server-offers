package com.tdp.ms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigServerOffersApplication {


	public static void main(String[] args) {
		SpringApplication.run(ConfigServerOffersApplication.class, args);
	}


}
